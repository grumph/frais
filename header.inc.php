<!DOCTYPE html>
<html lang="fr">
	<head>
		<title>Feuille de comptes - <?php echo $title;?></title>
		<link rel="icon" type="image/png" href="favicon.png">
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
		<script src="jquery.min.js" type="text/javascript"></script>
