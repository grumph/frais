$(document).ready(function() {
	$('.validator').hide();
	$('#btnDel').attr('disabled','disabled');
	$('.tempcred').hide();
	$('#isGroup').attr('checked', true);
	$('#cred1').children('[value="'+$('#debiteur').val()+'"]').attr('disabled',true);
	$('#cred1').children('[disabled!=true]:first').attr('selected','selected');
	if ($('#cred1').children('option[disabled!=true]').length == 1)
		$('#btnAdd').attr('disabled',true);
	$('#btnAdd').click(function() {
		var num     = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
		var newNum  = new Number(num + 1);      // the numeric ID of the new input field being added
		// create the new element via clone(), and manipulate it's ID using newNum value
		var newElem = $('#select' + num).clone().attr('id', 'select' + newNum);
		// manipulate the name/id values of the input inside the new element
		newElem.children(':first').attr('id', 'cred' + newNum).attr('name', 'cred' + newNum);
		$('#select'+num+' .tempcred').append(new Option($('#cred' + num).val(), $('#cred' + num).val(), true, true));
		$('#select'+num+' .tempcred').show();
		$('#cred' + num).hide();
		// remove the previous name from the new list
		newElem.children(':first').children('[value="'+$('#cred' + num).val()+'"]').attr('disabled',true);
		newElem.children(':first').children('[disabled!=true]:first').attr('selected','selected');
		// insert the new element after the last "duplicatable" input field
		$('#select' + num).after(newElem);
		$('#btnDel').attr('disabled',false); // enable remove
		if (newElem.children(':first').children('option[disabled!=true]').length == 1)
			$('#btnAdd').attr('disabled',true);
	});
	$('#btnDel').click(function() {
		var num = $('.clonedInput').length; // how many "duplicatable" input fields we currently have
		$('#select' + num).remove(); // remove the last element
		$('#btnAdd').attr('disabled','');
		num = num - 1
		$('#cred' + num).show();
		$('#select'+num+' .tempcred').hide();
		$('#select'+num+' .tempcred').children(':first').remove();
		if (num == 1)
			$('#btnDel').attr('disabled',true);
	});
	$('#isGroup').click(function() {
		$('#groupButtons').show();
	});
	$('#isTransaction').click(function() {
		$('#groupButtons').hide();
		$('.clonedInput:not(:first)').remove();
		$('.clonedInput select').show();
		$('.tempcred').empty();
		$('.tempcred').hide();
		$('#btnDel').attr('disabled', true);
		$('#btnAdd').attr('disabled', false);
		if ($('#cred1').children('option[disabled!=true]').length == 1)
			$('#btnAdd').attr('disabled',true);
	});
	$('#debiteur').change(function() {
		$('.clonedInput:not(:first)').remove();
		$('.clonedInput select').show();
		$('.tempcred').empty();
		$('.tempcred').hide();
		$('#btnDel').attr('disabled',true);
		$('#btnAdd').attr('disabled',false);
		$('#cred1').children().attr('disabled',false);
		$('#cred1').children('[value="'+$('#debiteur').val()+'"]').attr('disabled',true);
		$('#cred1').children('[disabled!=true]:first').attr('selected','selected');
		if ($('#cred1').children('option[disabled!=true]').length == 1)
			$('#btnAdd').attr('disabled',true);
	});
	$('#montant').priceFormat({ prefix: '€ ', centsSeparator: '.', thousandsSeparator: ' '});
});
