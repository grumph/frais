<?php

include('common.inc.php');
$title="Résultat";
include('header.inc.php');
include('menu.inc.php');
?>

<table>
	<tr>
		<th>Debiteur</th>
		<th>montant</th>
		<th>Crediteur</th>
		<th>Motif</th>
		<th>Date</th>
	</tr>

<?php
	$zetab = get_results($FILE);
	for($i=0;$i<sizeof($zetab);$i++){
		$creditors = explode("|",$zetab[$i]['to']);
		echo '<tr>';
		echo '<td>'.$zetab[$i]['from'].'</td>';
		echo '<td align="right">'.money_format('%i', $zetab[$i]['montant']).'</td>';
		echo '<td>';
		$pouet=0;
		foreach ($creditors as $value)
			echo (($pouet++ == 0) ?"":", ").$value;
		echo '</td>';
		echo '<td>'.$zetab[$i]['why'].'</td>';
		echo '<td align="right">'.$zetab[$i]['date'].'</td>';
		echo '</tr>';
		/* remove what the debitor paid from his total */
		if (isset($resultats) && array_key_exists($zetab[$i]['from'], $resultats))
			$resultats[$zetab[$i]['from']] -= $zetab[$i]['montant'];
		else
			$resultats[$zetab[$i]['from']] = 0 - $zetab[$i]['montant'];

		/* then add part of the price in creditors total */
		if (isset($resultats)) {
			foreach ($creditors as $value) {
				if (array_key_exists($value, $resultats))
					$resultats[$value] += $zetab[$i]['montant'] / sizeof($creditors);
				else
					$resultats[$value] = 0 + $zetab[$i]['montant'] / sizeof($creditors);
			}
		}
	}
  echo '</table>';

  echo '<footer>';
  foreach ($resultats as $key => $value) {
      if ($key != 'none') {
          echo '<section aria-label="', $key;
          if ($value < 0.1 && $value > -0.1) {
              echo ' ne doit rien faire';
          } else {
              echo ' doit ', ($value > 0) ? 'rembourser ' : 'récupérer ', money_format('%i', abs($value)) , '€';
          }
          echo '" ';
          if ($value > 0.01) {
              echo 'class="bad"';
          } else if ($value < -0.01) {
              echo 'class="good"';
          }
          echo ' >';
          echo '<summary>', $key, '</summary>';
          echo '<p>', ($value <= -0.1)? '-' : '', money_format('%i', abs($value)), '</p>';
          echo '</section>';
      }
  }
  echo '</footer>';

include 'footer.inc.php';

?>
