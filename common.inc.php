<?php
$FILE='frais.txt';

setlocale(LC_MONETARY, 'fr_FR');

function file_remove_bom($file) {
	$str = file_get_contents($file);
	$bom = pack("CCC", 0xef, 0xbb, 0xbf);
	if (0 == strncmp($str, $bom, 3)) {
		$str = substr($str, 3);
		$fp = fopen($file, 'w');
		fwrite($fp, $str);
		fclose($fp);
	}
}

function get_results($file){
	if (file_exists($file)) {
		if (!isset($_SESSION['checkFile']) || !$_SESSION['checkFile']) {
			file_remove_bom($file);
			$_SESSION['checkFile'] = true;
		}
		$tableau = file($file, FILE_SKIP_EMPTY_LINES);
		for($i=0; $i<sizeof($tableau); $i++){
			$infos = explode("#",$tableau[$i]);
			$res[$i]['from'] = $infos[0];
			$res[$i]['montant'] = $infos[1];
			$res[$i]['to'] = $infos[2];
			$res[$i]['why'] = $infos[3];
			$res[$i]['date'] = $infos[4];
		}
	}
	return $res;
}

function get_names($file){
	$zetab = get_results($file);
	for($i=0;$i<sizeof($zetab);$i++){
		if (!isset($res) || !array_key_exists($zetab[$i]['from'], $res))
			$res[$zetab[$i]['from']] = 42;
		foreach (explode("|",$zetab[$i]['to']) as $value)
			if ($value != 'none' && !array_key_exists($value, $res))
				$res[$value] = 42;
  }
  return $res;  
}

// If magic quotes are enabled, strip slashes from all user data
function stripslashes_recursive($var) {
  return (is_array($var) ? array_map('stripslashes_recursive', $var) : stripslashes($var));
}

if (get_magic_quotes_gpc()) {
  $_GET = stripslashes_recursive($_GET);
  $_POST = stripslashes_recursive($_POST);
  $_COOKIE = stripslashes_recursive($_COOKIE);
  $_REQUEST = stripslashes_recursive($_REQUEST);
}
?>
