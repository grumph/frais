<?php
include('common.inc.php');
$title="Ajout d'une transaction";

$valid = false;
if (isset($_POST['adding'])) {
	$valid = true;
}

if (!$valid) {
	function save_to_file($file){
		if (isset($_POST['debiteur']) && isset($_POST['montant']) && isset($_POST['motif']) && isset($_POST['crediteurs'])) {
			if ($fichier = fopen($file, 'a')) {
				fwrite($fichier, $_POST['debiteur'].'#'.$_POST['montant'].'#'.$_POST['crediteurs'].'#'.$_POST['motif'].'#'.date('d/m/y') ."\n");
				fclose($fichier);
			}
			else
				echo '<div>Erreur de fichier</div>';
		}
		else
			echo '<div>Transaction refusée</div>';
	}
	if (isset($_POST['valid'])) {
		save_to_file($FILE);
		header('Location: index.php');
	}


	$options = '';
	foreach (get_names($FILE) as $key => $value)
		$options .= '<option>'.$key.'</option>';
}
include('header.inc.php');
if (!$valid) {
?>
	<script src="jquery.price_format.1.7.min.js"></script>
	<script src="frais-add.js"></script>
<?php
}
include('menu.inc.php');


?>
<form method='post' action='frais-add.php'>
<?php

if ($valid) {
	if (isset($_POST['debiteur']) &&
		isset($_POST['montant']) &&
		isset($_POST['type']) &&
		isset($_POST['cred1']) &&
		isset($_POST['motif'])) {
			$pouet=1;
			if ($_POST['type']=="isGroup")
				$creditors = $_POST['debiteur'];
			else {
				$creditors = $_POST['cred1'];
				$pouet++;
			}
			while (isset($_POST['cred'.$pouet]))
				$creditors .= "|".$_POST['cred'.$pouet++];
	?>
		<div><label>Débiteur</label> <span><?php echo $_POST['debiteur']; ?></span><input type="hidden" name="debiteur" value="<?php echo $_POST['debiteur']; ?>" /></div>
		<?php $montant = preg_replace('/[^0-9.]/', '', $_POST['montant']); ?>
		<div><label>Montant</label> <span><?php echo money_format('%i', $montant); ?></span><input type="hidden" name="montant" value="<?php echo $montant; ?>" /></div>
		<div><label>Créditeurs</label> <span><?php echo str_replace('|', ', ', $creditors); ?></span><input type="hidden" name="crediteurs" value="<?php echo $creditors; ?>" /></div>
		<div><label>Motif</label> <span><?php echo $_POST['motif']; ?></span><input type="hidden" name="motif" value="<?php echo $_POST['motif']; ?>" /></div>
	<?php	}
	?>

	<button type="button" onclick="self.location.href='index.php'">Annuler</button>
	<button type='submit' name="valid">Confirmer</button>
</form>
<?php
}
else {
?>
		<div>
			<label for="debiteur">Débiteur: </label>
			<select id="debiteur" name='debiteur'><?php echo $options;?></select>
		</div>
		<div>
			<label for="montant">Montant: </label>
			<input id='montant' type='text' name='montant' value='0'>
		</div>
		<div>
			<label for="types">Type: </label>
			<span id="types">
				<input type="radio" id="isGroup" name="type" value="isGroup" /><label for="isGroup">Achat de groupe</label>
				<input type="radio" id="isTransaction" name="type" value="isTransaction" /><label for="isTransaction">Transaction</label>
			</span>
		</div>
		<div id="crediteurs">
			<label for="crediteurs">Créditeur(s): </label>
			<span id="select1" class="clonedInput">
				<select id="cred1" name="cred1"><?php echo $options;?></select>
				<select class="tempcred"></select>
			</span>
			<div  id="groupButtons">
				<button type="button" id="btnDel">-</button>
				<button type="button" id="btnAdd">+</button>
			</div>
		</div>
		<div>
			<label for="motif">Motif: </label>
			<input id="motif" type='text' name='motif' />
		</div>
		<div>
			<button type='submit' name='adding'>OK</button>
		</div>
	</form>
<?php
}

include 'footer.inc.php';

?>
