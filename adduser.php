<?php
include('common.inc.php');
$title="Ajout d'un utilisateur";

function check_username($username, $file) {
	$username = trim($username); // strip any white space
	$names = get_names($file);

	if (!$username)
		$response = array(
			'ok' => false,
			'msg' => "Veuillez indiquer un nom");
	else if (!preg_match('/^[a-z0-9.-_]+$/', $username))
		$response = array(
			'ok' => false,
			'msg' => "Caractères autorisés : lettres ou . - _");
	else if (array_key_exists($username, $names))
		$response = array(
			'ok' => false,
			'msg' => "Non disponible");
	else
		$response = array(
			'ok' => true,
			'msg' => "");

	return $response;
}

if (isset($_REQUEST['username'])){
	$error = check_username($_REQUEST['username'], $FILE);
	if ($_REQUEST['action'] == 'check_username' && isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
		echo (($error['ok'])?"1":"0").$error['msg'];
		exit;
	}
	if ($error['ok']){ // We can safely write everything in the file
		if ($fichier = fopen($FILE, 'a')) {
			fwrite($fichier, $_POST['username'].'#0#'.$_POST['username'].'#Création d\'utilisateur#'.date('d.m.y') ."\n");
			fclose($fichier);
			header('Location: index.php');
		}
		else
			echo "Fichier pas cool";
	}
}

include('header.inc.php');
?>
    <script type="text/javascript">
    <!--
    $(document).ready(function () {
        // Username validation logic
        var validateUsername = $('#validateUsername');
        $('#username').keyup(function () {
            var t = this;
            if (this.value != this.lastValue) {
                if (this.timer) clearTimeout(this.timer);
                validateUsername.html('<img src="ajax-loader.gif" />');

                this.timer = setTimeout(function () {
                    $.ajax({
                        url: 'adduser.php',
                        data: 'action=check_username&username=' + t.value,
                        type: 'post',
                        success: function (res) {
							if (res.substr(0, 1) == "1")
								validateUsername.attr('color', 'green');
							else
								validateUsername.attr('color', 'red');
                            validateUsername.html(res.substr(1,res.length - 1));
                        }
                    });
                }, 200);
                this.lastValue = this.value;
            }
        });
    });
    //-->
    </script>
  <?php include('menu.inc.php');?>
        <form action="adduser.php" method="post">
                <div>
                    <label for="username">Nom (a-z.-_)</label>
                    <input type="text" name="username" value="<?php echo $_REQUEST['username'];?>" id="username" />
                    <span id="validateUsername" style="color:red"><?php if (isset($error)) echo $error['msg']; ?></span>
                </div>
            </fieldset>
            <input type="hidden" name="action" value="register" />
            <button type="submit" name="register" id="register">Ajouter</button>
        </form>

<?php
    include 'footer.inc.php';
?>
